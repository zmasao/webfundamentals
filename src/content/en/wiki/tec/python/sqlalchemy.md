









#### Dialects

##### [MySQL](https://docs.sqlalchemy.org/en/latest/dialects/mysql.html)

```sh
yum install python-devel mysql-devel
pip install MySQL-Python
mysql+mysqldb://<user>:<password>@<host>[:<port>]/<dbname>
```





#### Query

#### Column

Database Date

```python
from sqlalchemy.sql import func
from_time = Column(DateTime, default=func.now(), doc=u'开始时间')
user.thru_time = func.now() + datetime.timedelta(minutes=30)
```





UUID

```python
from uuid import uuid4
session_id = Column(String(36), primary_key=True, default=uuid4)
```

Emum

```python
status = Column(Enum('Active', 'Inactive', 'Blocked', 'Suspended',  'Pending', name='user_status'), default="Inactive")
```

Lazy

```python
relationship("Address", uselist=False, lazy="noload")
```

​	noload 后 手动 load. [[ref](https://docs.sqlalchemy.org/en/latest/orm/query.html#sqlalchemy.orm.query.Query.join)]

```python
from sqlalchemy.orm import joinedload
users_list = db.session.query(User) \
                .options(joinedload(User.address)).order_by(User.id) \
                .limit(per_page).offset(per_page*(page-1)).all()
```

