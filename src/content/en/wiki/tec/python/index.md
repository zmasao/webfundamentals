project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Python

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>Python #}

# Python

[Python Tutorial](https://docs.python.org/3/tutorial/index.html)

[Python Version Features](https://docs.python.org/3/contents.html)


