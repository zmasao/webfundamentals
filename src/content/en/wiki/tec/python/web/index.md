project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Web

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>Python>Web #}


[Python WSGI Server Benchmark](https://blog.appdynamics.com/engineering/a-performance-analysis-of-python-wsgi-servers-part-2/)
[Python WSGI Server Benchmark Github](https://github.com/kubeup/python-wsgi-benchmark)

[WSGI Server Benchmark](http://smilejay.com/2018/02/wsgi-server-benchmarking/)
[WSGI Server Benchmark Source Code](https://github.com/smilejay/python)
