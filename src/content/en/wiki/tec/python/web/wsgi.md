project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Web

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>Python>Web #}

WSGI is not a server, a python module, a framework, an API or any kind of software. It is just an interface specification by which server and application communicate. Both server and application interface sides are specified in the PEP 3333. If an application (or framework or toolkit) is written to the WSGI spec then it will run on any server written to that spec.

Old [PEP 333 -- Python Web Server Gateway Interface v1.0](https://www.python.org/dev/peps/pep-0333/)
New [PEP 3333 -- Python Web Server Gateway Interface v1.0.1](https://www.python.org/dev/peps/pep-3333/)

[WSGI Sample](https://getfullstack.com/web_server/server_programming/python.html)
[WSGI Source](http://wsgi.readthedocs.io/en/latest/index.html)
[WebServers](https://docs.python.org/2/howto/webservers.html)
[WSGI Sample 2](http://wsgi.tutorial.codepoint.net)
