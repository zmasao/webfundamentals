project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Tornado

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>Python>Web #}


History: [toro](https://toro.readthedocs.io/en/stable/)

[Intrudotion to Tornado](http://demo.pythoner.com/itt2zh/index.html)



Tornado coroutines simplify asynchronous code at the cost of a performance overhead incurred by wrapping the [Stack Context](http://www.tornadoweb.org/en/stable/_modules/tornado/stack_context.html) to manage the lifecycle of the coroutine.

> The tornado.stack_context module is deprecated and will be removed in Tornado 6.0. The reason for this is that it is not feasible to provide this module's semantics in the presence of async def native coroutines. .ExceptionStackContext is mainly obsolete thanks to coroutines. .StackContext lacks a direct replacement although the new contextvars package (in the Python standard library beginning in Python 3.7) may be an alternative.
