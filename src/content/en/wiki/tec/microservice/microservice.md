

### Microservices VS Components

一派思想规定微服务应该能够独立运行，而一个组件应该在更大的软件系统结构中使用。Microservices通常可以用不同的编程语言编写，并采用不同的数据存储技术。Components如果它们带有不同的编程语言和数据存储技术，则不适合它们的母版应用程序。

 ![1552022713372](assets/1552022713372.png)

## 微服务架构优势

1 复杂度可控

在将应用分解的同时，规避了原本复杂度无止境的积累。每一个微服务专注于单一功能，并通过定义良好的接口清晰表述服务边界。

由于体积小、复杂度低，每个微服务可由一个小规模开发团队完全掌控，易于保持高可维护性和开发效率。

2 独立部署

由于微服务具备独立的运行进程，所以每个微服务也可以独立部署。当某个微服务发生变更时无需编译、部署整个应用。

由微服务组成的应用相当于具备一系列可并行的发布流程，使得发布更加高效，同时降低对生产环境所造成的风险，最终缩短应用交付周期。

3 技术选型灵活

微服务架构下，技术选型是去中心化的。每个团队可以根据自身服务的需求和行业发展的现状，自由选择最适合的技术栈。

由于每个微服务相对简单，所以需要对技术栈进行升级时所面临的风险就较低，甚至完全重构一个微服务也是可行的。

4 容错

当某一组件发生故障时，在单一进程的传统架构下，故障很有可能在进程内扩散，形成应用全局性的不可用。

在微服务架构下，故障会被隔离在单个服务中。若设计良好，其他服务可通过重试、平稳退化等机制实现应用层面的容错。

5 扩展

单块架构应用也可以实现横向扩展，就是将整个应用完整的复制到不同的节点。当应用的不同组件在扩展需求上存在差异时，微服务架构便体现出其灵活性，因为每个服务可以根据实际需求独立进行扩展。

简单来说，微服务是基于单体应用的新型架构模式，可以基于微服务更好的进行自动化测试、运维、监控，从而满足持续交付，最终实现高质量的用户价值。



## 微服务 VS 当前开发

微服务的开发模式和传统开发模式有着很大的不同，大致有以下几点：

-  **分工不同** ：现在一个组负责一个系统，一个人负责系统的一部分；微服务后可能是一人负责一个或多个系统。
-  **架构不同**：现在更多的从模块上拆分、前后端上拆分开发；微服务后将同时从横向纵向上拆分。
-  **部署方式不同**：现在是手工和半自动发布；微服务后得自动化运维。
-  **容灾不同**：现在的系统是一个整体，并且是单点运行；微服务后可以多点负载，还可以隔离故障避免系统整体宕机。
-  **团队结构不同** 2 pizza（6~10人）的小团队

![1552022881503](assets/1552022881503.png)





https://www.cnblogs.com/stulzq/p/8573828.html

http://blog.didispace.com/20160917-microservices-note/

http://yun.java1234.com/article/2320

https://github.com/budaLi/-Learning-materials-

http://yun.java1234.com/article/1574

## 微服务基础架构





构建微服务 2.0 技术栈的核心模块

![1548298134888](assets/1548298134888.png)



![1548298275472](assets/1548298275472.png)



### 服务框架选型

#### Java

**Spring Boot/Cloud** 由于Spring社区的影响力和Netflix的背书，目前可以认为是构建Java微服务的一个社区标准，Spring Boot目前在github上有超过20k星。基于Spring的框架本质上可以认为是一种RESTful框架（不是RPC框架），序列化协议主要采用基于文本的JSON，通讯协议一般基于HTTP。RESTful框架天然支持跨语言，任何语言只要有HTTP客户端都可以接入调用，但是客户端一般需要自己解析payload。目前Spring框架也支持Swagger契约编程模型，能够基于契约生成各种语言的强类型客户端，极大方便不同语言栈的应用接入，但是因为RESTful框架和Swagger规范的弱契约特性，生成的各种语言客户端的互操作性还是有不少坑的。

**Dubbo**[附录12.2]是阿里多年构建生产级分布式微服务的技术结晶，服务治理能力非常丰富，在国内技术社区具有很大影响力，目前github上有超过16k星。Dubbo本质上是一套基于Java的RPC框架，当当Dubbox扩展了Dubbo支持RESTful接口暴露能力。Dubbo主要面向Java 技术栈，跨语言支持不足是它的一个弱项，另外因为治理能力太丰富，以至于这个框架比较重，完全用好这个框架的门槛比较高，但是如果你的企业基本上投资在Java技术栈上，选Dubbo可以让你在服务框架一块站在较高的起点上，不管是性能还是企业级的服务治理能力，Dubbo都做的很出色。新浪微博开源的Motan（github 4k stars）也不错，功能和Dubbo类似，可以认为是一个轻量裁剪版的Dubbo。

**gRPC**是谷歌近年新推的一套RPC框架，基于protobuf的强契约编程模型，能自动生成各种语言客户端，且保证互操作。支持HTTP2是gRPC的一大亮点，通讯层性能比HTTP有很大改进。Protobuf是在社区具有悠久历史和良好口碑的高性能序列化协议，加上Google公司的背书和社区影响力，目前gRPC也比较火，github上有超过13.4k星。目前看gRPC更适合内部服务相互调用场景，对外暴露HTTP RESTful接口可以实现，但是比较麻烦（需要gRPC Gateway配合），所以对于对外暴露API场景可能还需要引入第二套HTTP RESTful框架作为补充。总体上gRPC这个东西还比较新，社区对于HTTP2带来的好处还未形成一致认同，建议谨慎投入，可以做一些试点。



- [Spring Cloud 学习案例](https://github.com/ityouknow/spring-cloud-examples)

#### Python

**[Nameko](https://github.com/nameko/nameko)**

**[zato](https://zato.io/docs/)**

### 运行时支撑服务选型



运行时支撑服务主要包括**服务注册中心**，**服务路由网关**和**集中式配置中心**三个产品。

#### 服务注册中心



**Eureka** ( Netflix )  如果采用Spring Cloud体系，则选择**Eureka**是最佳搭配，Eureka在Netflix经过大规模生产验证，支持跨数据中心，客户端配合Ribbon可以实现灵活的客户端软负载，Eureka目前在github上有超过4.7k星；

**Consul **也是不错选择，天然支持跨数据中心，还支持KV模型存储和灵活健康检查能力，目前在github上有超过11k星。



- [Spring Cloud构建微服务架构：服务注册与发现（Eureka、Consul）【Dalston版】](http://blog.didispace.com/spring-cloud-starter-dalston-1/)
- [springcloud(二)：注册中心Eureka](http://www.ityouknow.com/springcloud/2017/05/10/springcloud-eureka.html)
- [史上最简单的SpringCloud教程](https://blog.csdn.net/forezp/article/details/70148833)

#### 服务网关

**Zuul** ( Netflix ) 如果采用Spring Cloud体系，则选择**Zuul**是最佳搭配，Zuul在Netflix经过大规模生产验证，支持灵活的动态过滤器脚本机制，异步性能不足（基于Netty的异步Zuul迟迟未能推出正式版）。Zuul网关目前在github上有超过3.7k星。

**Kong** ( Nginx/OpenResty )  因为采用Nginx内核，Kong的异步性能较强，另外基于lua的插件机制比较灵活，社区插件也比较丰富，从安全到限流熔断都有，还有不少开源的管理界面，能够集中管理Kong集群。

#### 配置中心

**Spring Cloud Config** ( Spring )  算不上生产级，很多治理能力缺失，小规模场景可以试用。?????

**Apollo** ( Ctrip Corp )  携程经过生产级验证，具备高可用，配置实时生效（推拉结合），配置审计和版本化，多环境多集群支持等生产级特性，建议中大规模需要对配置集中进行治理的企业采用。Apollo目前在github上有超过3.4k星。

### 服务监控选型

主要包括**日志监控**，**调用链监控**，**Metrics监控**，**健康检查**和**告警通知**等产品。

#### 日志监控

**ELK**  目前可以认为是日志监控的标配，功能完善开箱即用，**Elasticsearch**目前在github上有超过28.4k星。

#### 告警通知

**Elastalert**  是Yelp开源的针对ELK的告警通知模块。

#### 调用链监控

Zipkin ( Twitter/OpenZipkin )

CAT ( 点评 )  在点评和国内多家互联网公司有落地案例，生产级特性和治理能力较完善，另外CAT自带告警模块。下面三款产品的评估表，供参考

Pinpoint ( Naver )

![1548315674087](assets/1548315674087.png)

#### Metrics监控

Metrics监控主要依赖于时间序列数据库(TSDB)

**OpenTSDB** ( StumbleUpon ) 较成熟，具有分布式能力可以横向扩展，但是相对较重，适用于中大规模企业

**KariosDB** ( Cassandra ) 基于Cassandra 它基本上是OpenTSDB针对Cassandra的一个改造版

**Argus**（github 0.29k星）是Salesforce开源的基于OpenTSDB的统一监控告警平台，支持丰富的告警函数和灵活的告警配置，OpenTSDB 本身不提供告警模块，Argus可以作为OpenTSDB的告警补充。

**InfluxDB**（github 12.4k stars）和**Prometheus**（github 14.3k stars）轻量级的TSDB，这些产品函数报表能力丰富，自带告警模块，但是分布式能力不足，适用于中小规模企业

**Grafana**（github 19.9k stars）是Metrics报表展示的社区标配。

#### 健康检查 & 告警产品

**Sensu**（github 2.7k stars），能够对各种服务（例如spring boot暴露的健康检查端点，时间序列数据库中的metrics，ELK中的错误日志等）定制灵活的健康检查(check)，然后用户可以针对check结果设置灵活的告警通知策略。Sensu在Yelp等公司有落地案例。

Esty开源的**411**（github 0.74k星）和Zalando的**ZMon** (github 0.15k星)，它们是分别在Esty和Zalando落地的产品，但是定制check和告警配置的使用门槛比较高，社区不热，建议有定制自研能力的团队试用。ZMon后台采用KairosDB存储，如果企业已经采用KariosDB作为时间序列数据库，则可以考虑ZMon作为告警通知模块。



### 服务容错选型

**Hystrix**（github 12.4k stars）把熔断、隔离、限流和降级等能力封装成组件，任何依赖调用（数据库，服务，缓存）都可以封装在Hystrix Command之内，封装后自动具备容错能力。Hystrix起源于Netflix的弹性工程项目，经过Netflix大规模生产验证，目前是容错组件的社区标准，github上有超12k星。其它语言栈也有类似Hystrix的简化版本组件。

Hystrix一般需要在应用端或者框架内埋点，有一定的使用门槛。对于采用集中式反向代理（边界和内部）做服务路由的公司，则可以集中在反向代理上做熔断限流，例如采用**nginx**（github 5.1k stars）或者**Kong**（github 11.4k stars）这类反向代理，它们都有插件支持灵活的限流容错配置。Zuul网关也可以集成Hystrix实现网关层集中式限流容错。集中式反向代理需要有一定的研发和运维能力，但是可以对限流容错进行集中治理，可以简化客户端。

### 后台服务选型

后台服务主要包括**消息系统**，**分布式缓存**，**分布式数据访问层**和**任务调度系统**。后台服务是一个相对比较成熟的领域，很多开源产品基本可以开箱即用。

#### 消息系统

**Kafka** 对于可靠性要求较高的业务场景，kafka其实也是可以胜任，但企业需要根据具体场景，对 Kafka的监控和治理能力进行适当定制完善，Allegro公司开源的**hermes**（github 0.3k stars）是一个可参考项目，它在Kafka基础上封装了适合业务场景的企业级治理能力。

**RocketMQ**（github 3.5k星）也是一个不错选择，具备更多适用于业务场景的特性.

**RabbitMQ**（github 3.6k星）是老牌经典的MQ，队列特性和文档都很丰富，性能和分布式能力稍弱，中小规模场景可选。

#### 缓存治理

**cachecloud** **如果倾向于采用客户端直连模式**（个人认为缓存直连更简单轻量）是一款不错的Redis缓存治理平台，提供诸如监控统计，一键开启，自动故障转移，在线伸缩，自动化运维等生产级治理能力，另外其文档也比较丰富。

**twemproxy**（github 7.5k stars） **采用中间层Proxy模式** Twitter 开源

**codis**（github 6.9k stars） **采用中间层Proxy模式**  CodisLab开源 。

#### 分布式数据访问层

**shardingjdbc**（github 3.5k stars）当当开源  是一个不错的选项，分库分表逻辑做在客户端jdbc driver中，**客户端直连数据库**比较简单轻量，建议中小规模场景采用。

**MyCAT**（github 3.6k stars）**数据库访问中间层proxy模式** 阿里Cobar演化出来的社区开源分库分表中间件 **proxy模式运维成本较高**

#### 任务调度系统

**xxl-job**（github 3.4k stars）徐雪里开源，部署简单轻量，大部分场景够用。

**elastic-job**（github 3.2k stars）也是一个不错选择，当当开源 相比xxl-job功能更强一些也更复杂。

### 服务安全选型

目前业界虽然有OAuth和OpenID connect等标准协议，但是各家具体实现的做法都不太一样，企业一般有很多特殊的定制需求，整个社区还没有形成通用生产级开箱即用的产品。有一些开源授权服务器产品，比较知名的如**Apereo CAS**（github 3.6k stars），JBoss开源的**keycloak**github 1.9 stars），**spring cloud security**等，大都是opinionated（一家观点和做法）的产品，同时因支持太多协议造成产品复杂，也缺乏足够灵活性。

建议基于OAuth和OpenID connect标准，在参考一些开源产品的基础上（例如Mitre开源的**OpenID-Connect-Java-Spring-Server**，github 0.62k stars），定制自研轻量级授权服务器。Wso2提出了一种微服务安全的参考方案，建议参考，该方案的关键步骤如下：

![1548315711182](assets/1548315711182.png)

1. 使用支持OAuth 2.0和OpenID Connect标准协议的授权服务器（个人建议定制自研）；

2. 使用API网关作为单一访问入口，统一实现安全治理；

3. 客户在访问微服务之前，先通过授权服务器登录获取access token，然后将access token和请求一起发送到网关；

4. 网关获取access token，通过授权服务器校验token，同时做token转换获取JWT token。

5. 网关将JWT Token和请求一起转发到后台微服务；

6. JWT中可以存储用户会话信息，该信息可以传递给后台的微服务，也可以在微服务之间传递，用作认证授权等用途；

7. 每个微服务包含JWT客户端，能够解密JWT并获取其中的用户会话信息。

8. 整个方案中，access token是一种by reference token，不包含用户信息可以直接暴露在公网上；JWT token是一种by value token，可以包含用户信息但不暴露在公网上。


### 服务部署平台选型

容器已经被社区接受为交付微服务的一种理想手段，可以实现不可变（immutable）发布模式。一个轻量级的基于容器的服务部署平台主要包括容器资源调度，发布系统，镜像治理，资源治理和IAM等模块。

#### 集群资源调度系统

屏蔽容器细节，将整个集群抽象成容器资源池，支持按需申请和释放容器资源，物理机发生故障时能够实现自动故障迁移(fail over)。目前Google开源的kubernetes，在Google背书和社区的强力推动下，基本已经形成市场领导者地位，github上有31.8k星，社区的活跃度已经远远超过了mesos（github 3.5k stars）和swarm等竞争产品，所以容器资源调度建议首选k8s。当然如果你的团队有足够定制自研能力，想深度把控底层调度算法，也可以基于mesos做定制自研。

#### 镜像治理

基于docker registry，封装一些轻量级的治理功能。vmware开源的harbor (github 3.5k stars)是目前社区比较成熟的企业级产品，在docker registry基础上扩展了权限控制，审计，镜像同步，管理界面等治理能力，可以考虑采用。

#### 资源治理

类似于CMDB思路，在容器云环境中，企业仍然需要对应用app，组织org，容器配额和数量等相关信息进行轻量级的治理。目前这块还没有生产级的开源产品，一般企业需要根据自己的场景定制自研。

#### 发布平台

面向用户的发布管理控制台，支持发布流程编排。它和其它子系统对接交互，实现基本的应用发布能力，也实现如蓝绿，金丝雀和灰度等高级发布机制。目前这块生产级的开源产品很少，Netflix开源的spinnaker（github 4.2k stars）是一个，但是这个产品比较复杂重量（因为它既要支持适配对接各种CI系统，同时还要适配对接各种公有云和容器云，使得整个系统异常复杂），一般企业建议根据自己的场景定制自研轻量级的解决方案。

#### IAM

是identity & access management的简称，对发布平台各个组件进行身份认证和安全访问控制。社区有不少开源的IAM产品，比较知名的有Apereo CAS（github 3.6k stars），JBoss开源的keycloak（github 1.9 stars）等。但是这些产品一般都比较复杂重量，很多企业考虑到内部各种系统灵活对接的需求，都会考虑定制自研轻量级的解决方案。

考虑到服务部署平台目前还没有端到端生产级解决方案，企业一般需要定制集成，下面给出一个可以参考的具备轻量级治理能努力的发布体系：

![1548315748304](assets/1548315748304.png)


简化发布流程如下：

1. 应用通过CI集成后生成镜像，用户将镜像推到镜像治理中心；
2. 用户在资产治理中心申请发布，填报应用，发布和配额相关信息，然后等待审批通过；
3. 发布审批通过，开发人员通过发布控制台发布应用；
4. 发布系统通过查询资产治理中心获取发布规格信息；
5. 发布系统向容器云发出启动容器实例指令；
6. 容器云从镜像治理中心拉取镜像并启动容器；
7. 容器内服务启动后自注册到服务注册中心，并保持定期心跳；
8. 用户通过发布系统调用服务注册中心调拨流量，实现蓝绿，金丝雀或灰度发布等机制；
9. 网关和内部微服务客户端定期同步服务注册中心上的服务路由表，将流量按负载均衡策略分发到新的服务实例上。

另外，持续交付流水线（CD Pipeline）也是微服务发布重要环节，这块主要和研发流程相关，一般需要企业定制，下面是一个可供参考的流水线模型，在镜像治理中心上封装一些轻量级的治理流程，例如只有通过测试环境测试的镜像才能升级发布到UAT环境，只有通过UAT环境测试的镜像才能升级发布到生产环境，通过在流水线上设置一些质量门，保障应用高质量交付到生产。

![1548315768906](assets/1548315768906.png)



### 附录链接

1. [Spring Boot](https://github.com/spring-projects/spring-boot)  
2. [Alibaba Dubbo](https://github.com/alibaba/dubbo)  
3. [Google gRPC](https://github.com/grpc/grpc)  
4. [NetflixOSS Eureka](https://github.com/Netflix/eureka)  
5. [Hashicorp Consul](https://github.com/hashicorp/consul)  
6. [NetflixOSS Zuul](https://github.com/Netflix/zuul)  
7. [Kong](https://github.com/Kong/kong)  
8. [Spring Cloud Config](https://github.com/spring-cloud/spring-cloud-config)  
9. [CTrip Apollo](https://github.com/ctripcorp/apollo)  
10. [ElasticSearch](https://github.com/elastic/elasticsearch)  
11. [Yelp Elastalert](https://github.com/Yelp/elastalert)  
12. [Dianping CAT](https://github.com/dianping/cat)  
13. [Zipkin](https://github.com/openzipkin/zipkin)  
14. [Naver Pinpoint](https://github.com/naver/pinpoint)  
15. [OpenTSDB](https://github.com/OpenTSDB/opentsdb)  
16. [KairosDB](https://github.com/kairosdb/kairosdb)  
17. [Argus](https://github.com/salesforce/Argus)  
18. [InfluxDB](https://github.com/influxdata/influxdb)  
19. [Prometheus](https://github.com/prometheus/prometheus)  
20. [Grafana](https://github.com/grafana/grafana)  
21. [Sensu](https://github.com/sensu/sensu)  
22. [Esty 411](https://github.com/etsy/411)  
23. [Zalando ZMon](https://github.com/zalando/zmon)  
24. [NetflixOSS Hystrix](https://github.com/Netflix/Hystrix)  
25. [Nginx](https://github.com/nginx/nginx)  
26. [Apache Kafka](https://github.com/apache/kafka)  
27. [Allegro Hermes](https://github.com/allegro/hermes)  
28. [Apache Rocketmq](https://github.com/apache/rocketmq)  
29. [Rabbitmq](https://github.com/rabbitmq/rabbitmq-server)  
30. [Sohutv CacheCloud](https://github.com/sohutv/cachecloud)  
31. [Twitter twemproxy](https://github.com/twitter/twemproxy)  
32. [CodisLab codis](https://github.com/CodisLabs/codis)  
33. [Dangdang Sharding-jdbc](https://github.com/shardingjdbc/sharding-jdbc)  
34. [MyCAT](https://github.com/MyCATApache/Mycat-Server)  
35. [Xxl-job](https://github.com/xuxueli/xxl-job)  
36. [Dangdang elastic-job](https://github.com/elasticjob/elastic-job-lite)  
37. [Apereo CAS](https://github.com/apereo/cas)  
38. [JBoss keycloak](https://github.com/keycloak/keycloak)  
39. [Spring cloud security](https://github.com/spring-cloud/spring-cloud-security)  
40. [OpenID-Connect-Java-Spring-Server](https://github.com/mitreid-connect/OpenID-Connect-Java-Spring-Server)  
41. [Google Kubernetes](https://github.com/kubernetes/kubernetes)  
42. [Apache Mesos](https://github.com/apache/mesos)  
43. [Vmware Harbor](https://github.com/vmware/harbor)  
44. [Netflix Spinnaker](https://github.com/spinnaker/spinnaker)  
45. [Microservices in Practice – Key Architecture Concepts of an MSA](https://wso2.com/whitepapers/microservices-in-practice-key-architectural-concepts-of-an-msa/)  



## 参考

[魏服务框架](https://wenku.baidu.com/view/a27e5c9848d7c1c709a1451c.html?rec_flag=default&sxts=1548316010550)

[微服务框架](http://blog.51cto.com/13545858/2133049)

[微服务案例](https://www.cnblogs.com/wintersun/p/6219259.html)

http://www.youmeek.com/microservice/

### 项目参考

[paascloud ](https://github.com/paascloud/paascloud-master)  Spring Cloud 实战项目  模拟商城，完整的购物流程、后端运营平台对前端业务的支撑，和对项目的运维，有各项的监控指标和运维指标。

[TCC柔性事务和EDA事件驱动示例](https://github.com/prontera/spring-cloud-rest-tcc) 基于Spring Cloud Netflix的TCC柔性事务和EDA事件驱动示例，结合Spring Cloud Sleuth进行会话追踪和Spring Boot Admin的健康监控，并辅以Hystrix Dashboard提供近实时的熔断监控

[spring-cloud-microservice-example](https://github.com/kbastani/spring-cloud-microservice-example)  Spring Cloud构建实用微服务架构的端到端云本机应用程序

[史上最简单的Spring Cloud教程源码](https://github.com/forezp/SpringCloudLearning)

[Spring-Cloud-Admin](https://github.com/wxiaoqi/Spring-Cloud-Admin) Cloud-Admin是国内首个基于Spring Cloud微服务化开发平台，具有统一授权、认证后台管理系统，其中包含具备用户管理、资源权限管理、网关API管理等多个模块，支持多业务系统并行开发，可以作为后端服务的开发脚手架。代码简洁，架构清晰，适合学习和直接项目中使用。核心技术采用Spring Boot2以及Spring Cloud Gateway相关核心组件，前端采用vue-element-admin组件。

### MicroServices Framework and Tools



#### Netflix OSS



##### 构建 - Netbula



##### 部署和交付 - Spinnaber and Aminator



##### 服务注册和发现 - Eureka



##### 服务沟通 - Ribbon



##### 电路断路器 - Hystrix



##### 边缘（代理）服务器 - Zuul



##### 业务监控 - Atlas



##### 可靠性监控服务 - Simian Army



##### AWS 资源监控 = Edda



##### 主机性能监控 - Vector



##### 分布式配置管理 - Archaius



##### Apapche Mesos 调度其 - Fenzo



##### 陈本和云利用率 - Ice



##### 其他安全工具 - Scumblr and FIDO



 





































