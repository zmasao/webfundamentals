# Auth





### JWT

#### 1. What are JSON Web Tokens?

JSON Web Tokens（JWT）是一个开放标准（[RFC 7519](https://tools.ietf.org/html/rfc7519)），它定义了一种紧凑且独立的方式，可以在各方之间作为JSON对象安全地传输信息。此信息可以通过数字签名进行验证和信任。JWT可以使用秘密（使用**HMAC**算法）或使用**RSA**或**ECDSA**的公钥/私钥对进行**签名**。

虽然JWT可以加密以在各方之间提供保密，但我们将专注于*签名*令牌。签名令牌可以验证其中包含的声明的*完整性*，而加密令牌则*隐藏*其他方的声明。当使用公钥/私钥对签署令牌时，签名还证明只有持有私钥的一方是签署私钥的一方。

以下是JSON Web令牌有用的一些场景：

- **授权**：这是使用JWT的最常见方案。一旦用户登录，每个后续请求将包括JWT，允许用户访问该令牌允许的路由，服务和资源。Single Sign On是一种现在广泛使用JWT的功能，因为它的开销很小，并且能够在不同的域中轻松使用。
- **信息交换**：JSON Web令牌是在各方之间安全传输信息的好方法。因为JWT可以签名 - 例如，使用公钥/私钥对 - 您可以确定发件人是他们所说的人。此外，由于使用标头和有效负载计算签名，您还可以验证内容是否未被篡改。

#### 2. JWT Structure

In its compact form, JSON Web Tokens consist of three parts separated by dots (`.`), which are:

- Header
- Payload
- Signature

Therefore, a JWT typically looks like the following.

```
aaaaaa.bbbbbb.cccccc
```

[![json-web-token-overview](https://cask.scotch.io/2014/11/json-web-token-overview1.png)](https://cask.scotch.io/2014/11/json-web-token-overview1.png)

##### 1. Header

标头*通常*由两部分组成：令牌的类型，即JWT，以及正在使用的签名算法，例如HMAC SHA256或RSA。

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

然后，这个JSON被**base64encode**编码为**Base64Url**，形成JWT的第一部分。

##### 2. Payload

令牌的第二部分是Payload，其中包含**claims**。claims是关于实体（通常是用户）和其他数据的声明。claims有三种类型： ***registered*, *public*, and *private* claims 。**

- Registered Claims :

  `iss`: The issuer of the token

  `sub`: The subject of the token

  `aud`: The audience of the token

  `exp`: This will probably be the registered claim most often used. This will define the expiration in NumericDate value. The expiration MUST be after the current date/time.

  `nbf`: Defines the time before which the JWT MUST NOT be accepted for processing

  `iat`: The time the JWT was issued. Can be used to determine the age of the JWT

  `jti`: Unique identifier for the JWT. Can be used to prevent the JWT from being replayed. This is helpful for a one time use token.

+ Public Claims :

+ Private Claims

示例有效负载有两个已注册的声明（`iss`，和`exp`）和两个公开声明（`name`，`admin`）

```json
{
  "iss": "scotch.io",
  "exp": 1300819380,
  "name": "Chris Sevilleja",
  "admin": true
}
```

然后，这个JSON被**base64encode**编码为**Base64Url**，形成JWT的第二部分。

##### 3. Signature

签名由以下组件的哈希组成：

- the header
- the payload
- secret

这就是我们如何获得 JWT 的第三部分：

```javascript
var encodedString = base64UrlEncode(header) + "." + base64UrlEncode(payload);
HMACSHA256(encodedString, 'your-256-bit-secret');
```

**secret** 是服务器持有的签名。这是我们的服务器能够验证现有令牌并签署新令牌的方式。

#### 3. All

输出是三个由点分隔的 Base64-URL 字符串, 您可以使用 [jwt.io Debugger](http://jwt.io/) 来解码，验证和生成 JWT

![1546001520535](assets/1546001520535.png)

#### 4. JWT  Work

一般情况下，您不应该将令牌保留的时间超过要求。每当用户想要访问受保护的路由或资源时，通常在 **Authorization** 标头中, **user agent** 应该使用 **Bearer** 模式发送 JWT

```ini
Authorization: Bearer <token>
```

在某些情况下，这可以是无状态授权机制。服务器的受保护路由将检查`Authorization`标头中的有效JWT ，如果存在，则允许用户访问受保护资源。如果JWT包含必要的数据，则可以减少查询数据库以进行某些操作的需要，尽管可能并非总是如此。

如果在标`Authorization`头中发送令牌，则跨域资源共享（CORS）将不会成为问题，因为它不使用cookie。

下图显示了如何获取JWT并用于访问API或资源：

![1546002038380](assets/1546002038380.png)

1. 应用程序或客户端向授权服务器请求授权。这是通过其中一个不同的授权流程执行的。例如，典型的[OpenID Connect](http://openid.net/connect/)兼容Web应用程序将`/oauth/authorize`使用[授权代码流](http://openid.net/specs/openid-connect-core-1_0.html#CodeFlowAuth)通过端点。
2. 授予授权后，授权服务器会向应用程序返回访问令牌。
3. 应用程序使用访问令牌来访问受保护资源（如API）。

请注意，使用签名令牌，令牌中包含的所有信息都会向用户或其他方公开，即使他们无法更改。这意味着您不应该在令牌中放置秘密信息。

#### 5. 为什么要使用JSON Web令牌？

让我们来谈谈与**简单Web令牌（SWT）**和**安全断言标记语言令牌（SAML）**相比，**JSON Web令牌（JWT）**的好处。

由于JSON比XML更简洁，因此在编码时它的大小也更小，使得JWT比SAML更紧凑。这使得JWT成为在HTML和HTTP环境中传递的不错选择。

在安全方面，SWT只能使用HMAC算法通过共享密钥对称签名。但是，JWT和SAML令牌可以使用X.509证书形式的公钥/私钥对进行签名。与签名JSON的简单性相比，使用XML数字签名对XML进行签名而不会引入模糊的安全漏洞非常困难。

JSON解析器在大多数编程语言中很常见，因为它们直接映射到对象。相反，XML没有自然的文档到对象映射。这使得使用JWT比使用SAML断言更容易。

关于使用，JWT用于互联网规模。这突出了在多个平台（尤其是移动平台）上轻松进行JSON Web令牌的客户端处理。

#### 6. Reference

- [The Anatomy of a JSON Web Token](https://scotch.io/tutorials/the-anatomy-of-a-json-web-token#payload)
- [Token-Based Authentication With Flask](https://realpython.com/token-based-authentication-with-flask/#introduction)



