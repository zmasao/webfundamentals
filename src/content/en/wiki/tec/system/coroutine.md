project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Coroutine

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>System>Coroutine #}

# Coroutine

协程（语言级构造）是`nonpreemptive multitasking`通过允许多个入口点在某些位置暂停和恢复执行来概括子程序的计算机程序组件。

> Coroutines are computer-program components that generalize subroutines for non-preemptive multitasking, by allowing multiple entry points for suspending and resuming execution at certain locations. Coroutines are well-suited for implementing familiar program components such as cooperative tasks, exceptions, event loops, iterators, infinite lists and pipes. ———— Form [Wikipedia Coroutine](https://en.wikipedia.org/wiki/Coroutine)

Warning: **协程可以处理IO密集型程序的效率问题，但是处理CPU密集型不是它的长处，如要充分发挥CPU利用率可以结合多进程+协程。**

## Definition

[questions](https://blog.csdn.net/delphiwcdj/article/details/52792944)

### yield

[yield]()：在计算机科学中，`yield`是在多线程期间在计算机程序中发生的动作，强制处理器到`relinquish control`当前运行的线程，并将其发送到`the end of the running queue`相同的调度优先级。

### Threading vs Coroutine

- **非抢占式、无需线程上下文切换的开销**:
  协同程序通过允许多个入口点在某些位置暂停和恢复执行来概括用于非抢占式多任务的子程序,在执行函数A时，可以随时暂停，去执行函数B，然后暂停继续执行函数A,这一过程并不是函数调用。这种方式不需要操作系统做进程或线程的切换，而是拥有自己的寄存器上下文和栈,[控制流](#controller_flow)是显式让出的，而不是由调度器来抢占式调度的，自行来控制任务的切换，以节省和恢复最小的上下文和栈，当任务执行过程中需要进行IO等待等动作时，任务则将其所占据的进程或线程释放，以便其他任务使用这个进程或线程。由于多数都coroutine采用了Actor Model来实现，Actor Model简单来说就是每个任务就是一个Actor，Actor之间通过消息传递的方式来进行交互，而不采用共享的方式，Actor可以看做是一个轻量级的进程或线程，通常在一台4G内存的机器上，创建几十万个Actor是毫无问题的，Actor支持Continuations。
  ![](images/context_switching.jpg)
- 无需数据操作锁定及同步的开销
- 方便切换控制流，简化编程模型
- 高并发+高扩展性+低成本：一个CPU支持上万的协程都不是问题。所以很适合用于高并发处理。

TODO:

- 有栈协程
- 无栈协程

## Controller Flow

### Simple Call

In simple call, the caller exerts total control over the callee. That is, there is a master/slave relationship between them.

![](images/simple_call.png)

### Parallel Processing

Parallel processing, also called concurrent processing, refers to the concurrent execution of two or more subprograms in order to solve a specific problem. Parallelism may be real, that is, executing simultaneously in real time on multiple processors, or virtual, simulated parallelism on a single central processor.

![](images/parallel_control.png)

### Coroutines

If we remove the requirement of caller having total control over callee, we get mutual control. Subprograms exerting mutual control over each other are called coroutines. Coroutines can each call the other. But, different from an ordinary call, a call by B to A actually transfers control to the point in subprogram A where A last called B. A different keyword, such as resume, may be used in place of call.

![](images/coroutine.png)


## Implemention


### Python 2.5 laster

[PEP 342 -- Coroutines via Enhanced Generators](https://www.python.org/dev/peps/pep-0342/) : Motivation

Python的生成器函数几乎是协同程序 - 但并不完全 - 因为它们允许暂停执行以生成值，但是不提供在执行恢复时传递的值或异常。它们也不允许在try / finally块的try部分中暂停执行，因此使中止的协程很难在其自身之后进行清理。

此外，当其他函数正在执行时，生成器不能产生控制，除非这些函数本身表示为生成器，并且外部生成器被写入以响应内部生成器产生的值而生成。这使得甚至相对简单的用例（如异步通信）的实现变得复杂，因为调用任何函数要么需要生成器阻塞（即无法控制），要么必须在每个需要的函数调用周围添加许多样板循环代码。

然而，如果有可能将值或异常传递到在它被暂停的generator，一个简单的协同例程调度或Trampolines
功能将让协程调用彼此而不阻塞。


    def coroutine(func):
        def wrapper(*args,**kw):
            gen = func(*args, **kw)
            try:
                gen.next()
            except StopIteration: pass
            return gen
        wrapper.__name__ = func.__name__
        wrapper.__dict__ = func.__dict__
        wrapper.__doc__  = func.__doc__
        return wrapper

    @coroutine
    def grep(pattern):
        print "Looking for %s" % pattern
        while True:
            line = (yield)
            if pattern in line:
                print line,

    g = grep("hello")
    g.send("hello world")
    g.send("test hello")
    g.send("test world")

<iframe width="100%" height="500px" src="http://www.dabeaz.com/coroutines/Coroutines.pdf"></iframe>

#### Comparison with generators

一个真正的协程能够控制代码什么时候继续执行。而一个Generator执行遇到一个yield表达式 或者语句，会将执行控制权转移给调用者。**但是，仍然可以在生成器能力之上借助顶级调度程序（本质上是一个 [Trampoline](https://en.wikipedia.org/wiki/Trampoline_(computing)): Trampolines (sometimes referred to as indirect jump vectors) are memory locations holding addresses pointing to interrupt service routines, I/O routines, etc. Execution jumps into the trampoline and then immediately jumps out, or bounces, hence the term trampoline. They have many uses.）实现协同程序**，将控制权明确地传递给由生成器传回的标记所标识的子生成器

> However, it is still possible to implement coroutines on top of a generator facility, with the aid of a top-level dispatcher routine (a trampoline, essentially) that passes control explicitly to child generators identified by tokens passed back from the generators.  -- [Wikipedia Coroutine](https://en.wikipedia.org/wiki/Coroutine#Comparison_with_generators)

If you use yield more generally, you get a coroutine

Key difference. Generators pull data through the pipe with iteration. Coroutines push data into the pipeline with send().


### Python 3.3 yield from

[PEP 380 -- Syntax for Delegating to a Subgenerator](https://www.python.org/dev/peps/pep-0380/)


    # 异步IO例子：适配Python3.3 laster，使用asyncio库
    @asyncio.coroutine
    def hello(index):                   # 通过装饰器asyncio.coroutine定义协程
        print('Hello world! index=%s, thread=%s' % (index, threading.currentThread()))
        yield from asyncio.sleep(1)     # 模拟IO任务
        print('Hello again! index=%s, thread=%s' % (index, threading.currentThread()))

    loop = asyncio.get_event_loop()     # 得到一个事件循环模型
    tasks = [hello(1), hello(2)]        # 初始化任务列表
    loop.run_until_complete(asyncio.wait(tasks))    # 执行任务
    loop.close()                        # 关闭事件循环列表

### Python 3.5 async def

It is proposed to make coroutines a proper standalone concept in Python, and introduce new supporting syntax.

[PEP 492 -- Coroutines with async and await syntax](https://www.python.org/dev/peps/pep-0492/)


    import asyncio

    async def compute(x, y):
        print("Compute %s + %s ..." % (x, y))
        await asyncio.sleep(1.0)
        return x + y

    async def print_sum(x, y):
        result = await compute(x, y)
        print("%s + %s = %s" % (x, y, result))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(print_sum(1, 2))
    loop.close()

async关键字将一个函数声明为协程函数，函数执行时返回一个协程对象。

await关键字将暂停协程函数的执行，等待异步IO返回结果。

EventLoop是一个程序结构，用于等待和发送消息和事件。简单说，就是在程序中设置两个线程：一个负责程序本身的运行，称为"主线程"；另一个负责主线程与其他进程（主要是各种I/O操作）的通信，被称为"Event Loop线程"（可以译为"消息线程"）。

![](images/event_loop.jpg)


    async def get(url):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                print(url, resp.status)
                print(url, await resp.text())

    loop = asyncio.get_event_loop()     # 得到一个事件循环模型
    tasks = [                           # 初始化任务列表
        get("http://zhushou.360.cn/detail/index/soft_id/3283370"),
        get("http://zhushou.360.cn/detail/index/soft_id/3264775"),
        get("http://zhushou.360.cn/detail/index/soft_id/705490")
    ]
    loop.run_until_complete(asyncio.wait(tasks))    # 执行任务
    loop.close()                        # 关闭事件循环列表

### Greenlet

Greenlet 手动切换；Gevent 自动切换，封装了Greenlet greenlet是一个用C实现的协程模块，相比与python自带的yield，它可以使你在任意函数之间随意切换，而不需把这个函数先声明为generator。 但没有解决一个问题，就是遇到IO操作，自动切换

    from greenlet import greenlet

    def test1():
        print(12)
        gr2.switch()
        print(34)
        gr2.switch()

    def test2():
        print(56)
        gr1.switch()
        print(78)

    gr1 = greenlet(test1)
    gr2 = greenlet(test2)
    gr1.switch()

### Gevent

`Gevent` 是一个第三方库，可以轻松通过`gevent`实现并发同步或异步编程，在`gevent`中用到的主要模式是`Greenlet`, 它是以`C`扩展模块形式接入`Python`的轻量级协程。 `Greenlet`全部运行在主程序操作系统进程的内部，但它们被协作式地调度。 --[Reference](http://c.isme.pub/2018/03/16/python-Coroutine/)

    import gevent

    def foo():
        print("Running in foo")
        gevent.sleep(2)
        print("swich to foo again")
    # 来回切换，直到sleep结束
    def bar():
        print("Running in bar")
        gevent.sleep(1)
        print("swich back to bar")

    def func3():
        print("Running in func3")
        gevent.sleep(0)  # 只触发一次切换操作
        print("swich func3 again")

    gevent.joinall([
        gevent.spawn(foo),  # 生成
        gevent.spawn(bar),
        gevent.spawn(func3),
    ])

遇到IO阻塞时会自动切换任务

    from urllib import request
    import gevent, time
    # 注意！：Gevent检测不到urllib的io操作，还是串行的，让它知道就需要打补丁
    from gevent import monkey
    monkey.patch_all()  # 把当前程序的所有IO操作给我做单独的做上标记

    def f(url):
        print("Get %s" %url)
        resp = request.urlopen(url)
        data = resp.read()
        # with open("url.html", 'wb') as f:
        #     f.write(data)
        print("%d bytes received from %s" %(len(data), url))

    print("异步时间统计中……")  # 协程实现
    async_start_time = time.time()
    gevent.joinall([
        gevent.spawn(f, "https://www.python.org"),
        gevent.spawn(f, "https://www.yahoo.com"),
        gevent.spawn(f, "https://github.com"),
    ])
    print("\033[32;1m异步cost：\033[0m",time.time()-async_start_time)
    #------------------------以下只为对比效果---------------------------
    print("同步步时间统计中……")
    urls = [
        "https://www.python.org",
        "https://www.yahoo.com",
        "https://github.com",
    ]
    start_time = time.time()
    for url in urls:
        f(url)
    print("\033[32;1m同步cost：\033[0m",time.time()-start_time)

## Comp

### Lifecycle

[Reference](https://www.jianshu.com/p/779289f5e340)
[Reference](https://blog.csdn.net/andybegin/article/details/77884645)
[Reference](https://blog.csdn.net/xfgryujk/article/details/80854750)
[Reference](https://cuiqingcai.com/6160.html)
[Reference](http://python.jobbole.com/87310/)

Eventloop
Task
...



## Example

Here is a mock of a distributed computing system with a cache, load balancer, and three workers.
- [Reference](https://blog.thumbtack.net/python-coroutines/)

    def coroutine(f):
        def wrapper(*arg, **kw):
            c = f(*arg, **kw)
            c.send(None)
            return c
        return wrapper

    @coroutine
    def logger(prefix="", next=None):
        while True:
            message = yield
            print("{0}: {1}".format(prefix, message))
            if next:
                next.send(message)

    @coroutine
    def cache_checker(cache, onsuccess=None, onfail=None):
        while True:
            request = yield
            if request in cache and onsuccess:
                onsuccess.send(cache[request])
            elif onfail:
                onfail.send(request)

    @coroutine
    def load_balancer(*workers):
        while True:
            for worker in workers:
                request = yield
                worker.send(request)

    @coroutine
    def worker(cache, response, next=None):
        while True:
            request = yield
            cache[request] = response
            if next:
                next.send(response)

    cache = {}
    response_logger = logger("Response")
    cluster = load_balancer(
        logger("Worker 1", worker(cache, 1, response_logger)),
        logger("Worker 2", worker(cache, 2, response_logger)),
        logger("Worker 3", worker(cache, 3, response_logger)),
    )
    cluster = cache_checker(cache, response_logger, cluster)
    cluster = logger("Request", cluster)

    if __name__ == "__main__":
        from random import randint

        for i in range(20):
            cluster.send(randint(1, 5))

![](images/example_destribute.png)

If you want to understand why you should love coroutines, try to implement this system without them. Of course, you can implement some classes to store state in the attributes and do work using send method:

    class worker(object):

        def __init__(self, cache, response, next=None):
            self.cache = cache
            self.response = response
            self.next = next

        def send(self, request):
            self.cache[request] = self.response
            if self.next:
                self.next.send(self.response)

see library [CoPipes](https://pypi.python.org/pypi/CoPipes)

## References

- [多线程的代价及上下文切换](https://blog.csdn.net/big_bit/article/details/52122253)
- [Context switch for wiki](https://en.wikipedia.org/wiki/Context_switch)
- [Context switch for linfo](http://www.linfo.org/context_switch.html)
- [C Coroutine Sample](https://www.oschina.net/translate/coroutines-in-c)
- [3.3 PEP List](https://docs.python.org/3/whatsnew/3.3.html#pep-380)
