

## Spring boot Reference



| [Legal](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/colophon.html#colophon) | Legal information.                                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Documentation Overview](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/documentation-overview.html#boot-documentation) | About the Documentation, Getting Help, First Steps, and more. |
| [Getting Started](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/getting-started.html#getting-started) | Introducing Spring Boot, System Requirements, Servlet Containers, Installing Spring Boot, Developing Your First Spring Boot Application |
| [Using Spring Boot](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/using-spring-boot.html#using-boot) | Build Systems, Structuring Your Code, Configuration, Spring Beans and Dependency Injection, and more. |
| [Spring Boot Features](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/spring-boot-features.html#boot-features) | Profiles, Logging, Security, Caching, Spring Integration, Testing, and more. |
| [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/production-ready-features.html#production-ready) | Monitoring, Metrics, Auditing, and more.                     |
| [Deploying Spring Boot Applications](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/deployment.html#deployment) | Deploying to the Cloud, Installing as a Unix application.    |
| [Spring Boot CLI](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/spring-boot-cli.html#cli) | Installing the CLI, Using the CLI, Configuring the CLI, and more. |
| [Build Tool Plugins](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/build-tool-plugins.html#build-tool-plugins) | Maven Plugin, Gradle Plugin, Antlib, and more.               |
| [“How-to” Guides](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/howto.html#howto) | Application Development, Configuration, Embedded Servers, Data Access, and many more. |
| [Appendices](https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/appendix.html#appendix) | Properties, Metadata, Configuration, Dependencies, and more. |



### Documentation Overview

#### 1. About the Documentation

The latest copy of the reference documentation is available at [docs.spring.io/spring-boot/docs/current/reference](https://docs.spring.io/spring-boot/docs/current/reference).

#### 2. System Requirements

Spring Boot 2.1.1.RELEASE requires [Java 8](https://www.java.com/) and is compatible up to Java 11 (included). [Spring Framework 5.1.3.RELEASE](https://docs.spring.io/spring/docs/5.1.3.RELEASE/spring-framework-reference/) or above is also required.

Explicit build support is provided for the following build tools:

| Build Tool | Version |
| ---------- | ------- |
| Maven      | 3.3+    |
| Gradle     | 4.4+    |

##### 2.1. Servlet Containers

Spring Boot supports the following embedded servlet containers:

| Name         | Servlet Version |
| ------------ | --------------- |
| Tomcat 9.0   | 4.0             |
| Jetty 9.4    | 3.1             |
| Undertow 2.0 | 4.0             |

You can also deploy Spring Boot applications to any Servlet 3.1+ compatible container.

#### 3. Build System

##### Maven

###### Inheriting the Starter Parent

To configure your project to inherit from the `spring-boot-starter-parent`, set the `parent` as follows:

```xml
<!-- Inherit defaults from Spring Boot -->
<parent>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-parent</artifactId>
	<version>2.1.1.RELEASE</version>
	<relativePath />
</parent>
```
###### Using Spring Boot without the Parent POM

If you do not want to use the `spring-boot-starter-parent`, you can still keep the benefit of the dependency management (but not the plugin management) by using a `scope=import` dependency, as follows:

```xml
<dependencyManagement>
	<dependencies>
		<dependency>
			<!-- Import dependency management from Spring Boot -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-dependencies</artifactId>
			<version>2.1.1.RELEASE</version>
			<type>pom</type>
			<scope>import</scope>
		</dependency>
	</dependencies>
</dependencyManagement>
```
##### First Spring Boot Application

```java
@RestController
@EnableAutoConfiguration
public class Example {

	@RequestMapping("/")
	String home() {
		return "Hello World!";
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Example.class, args);
	}

}
```









https://www.ctolib.com/ityouknow-spring-cloud-examples.html

https://www.ctolib.com/spring-cloud-netflix-example.html

https://www.ctolib.com/antares.html

https://github.com/prontera/spring-cloud-rest-tcc



https://www.ctolib.com/abixen-abixen-platform.html

分布式事务框架    https://github.com/codingapi/tx-lcn

https://www.ctolib.com/dyc87112-spring-cloud-config-admin.html

微服务的相关规范  https://www.ctolib.com/dqeasycloud-easy-cloud.html



redis的分布式锁  https://www.ctolib.com/kekingcn-spring-boot-klock-starter.html









