project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Automatic IT

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Wiki>Automatic IT#}

# Infrastructure

Managing Infrastructure As Code (IaC) automation. IaC means deploying and managing infrastructure for computing, including virtual servers and bare-metal servers. Definition files are used instead of physical hardware management.

    [Automatic IT](https://blog.appdynamics.com/engineering/deploying-at-scale-chef-puppet-ansible-fabric-and-saltstack/)

### SaltStack
SaltStack是一个基于Python的开源平台，用于管理和配置云基础架构。它是由Thomas S. Hatch使用ZeroMQ开发的，用于创建更好的高速收集和执行数据的工具。最初于2011年发布，2014年增加了Reliable Queuing Transport（RAET）。该项目随后通过包括几家大型企业在内的合作伙伴关系开发。SaltStack是从头开始构建的，具有高度模块化和灵活性，能够适应各种应用。它创建了Python模块，每个模块管理Salt系统的不同部分。您可以分离和修改模块以满足项目的需要。每个模块都旨在处理特定的操作。六种模块包括：

- Execution modules which offer functions for directly executing the remote execution engine as well as help manage portability and core API functions.
- Grains detect system static information and keep it in RAM for fast access.
- State modules represent the back end, executing code to configure or change a target system.
- Renderer modules pass information to the state system.
- Returners modules manage the return locations associated with remote execution calls.
- Runners are convenience apps.

SaltStack created a buzz early on by capturing the `2014 InfoWorld Technology of the Year Award` as well as the `2013 TechCrunch Award` for Most Exciting Project.

Organizations and companies using SaltStack include `Adobe, Jobspring Partners, Dealertrack Holdings, JumpCloud and International Game Technology`.

