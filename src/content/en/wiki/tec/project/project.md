





[TOC]

## 后端



### 分布式

#### 分布式计算

#### 分布式存储

#### 分布式系统通讯基础

#### 分布式实时处理



## 前端



### 客户端网络 IP 地址



http://whois.pconline.com.cn/ipJson.jsp?ip=

### 登录



#### 验证码

[reCAPTCHA](https://developers.google.com/recaptcha/intro) 是Google公司的验证码服务，方便快捷，改变了传统验证码需要输入n位失真字符的特点。Google会收集一些鼠标轨迹、网络信息、浏览器信息等等，依靠后端的神经网络判断是机器还是人，绝大多数验证会一键通过，无需像传统验证码一样。



获取 API key  https://www.google.com/recaptcha/admin



客户端：

```javascript
<script src='https://recaptcha.google.cn/recaptcha/api.js?render=6LeDhYgUAAAAAKkdRZBjE5lOAkIdotgsnvzjt7S4'></script>

grecaptcha.ready(function() {
    grecaptcha.execute('6LeDhYgUAAAAAKkdRZBjE5lOAkIdotgsnvzjt7S4', {action: 'action_name'})
        .then(function(token) {
        var valicode_token = token;
    });
});

HttpRequest.post("/auth", {
    "valicode_token":valicode_token
},function(code, message, data){})
```

服务端：

```python
def auth():
	private_key = '6LeDhYgUAAAAAKTcQvu4fLjuX_b-NRaXEvZx9vKF'
    valicode_token = request.json.get("valicode_token")
    remote_addr = request.environ.get('X-Forwarded-For', \
                  request.environ.get('HTTP_X_REAL_IP',  request.remote_addr))
    verify_url = 'https://recaptcha.google.cn/recaptcha/api/siteverify?lh=zh'

    data = {
        'secret': private_key,
        'remoteip': remote_addr,
        'response': valicode_token
    }
    logger.debug('reCaptcha server verify start....')
    try:
        response = grequests.post(verify_url, data = data).send().response
        response_content = response.json()
        logger.debug('reCaptcha server verify reponse:{0}'.format(response_content))
        if not response_content['success']:
            raise Exception("非法操作,请刷新界面后再试！")
    except Exception as e:
        logger.error('reCaptcha server verify fail....')
        logger.error(traceback.format_exc())
        raise e
    logger.debug('reCaptcha server verify end....')

```



引入资源的时候需要把链接替换为国内资源：

- https://recaptcha.google.cn/recaptcha/api.js
- https://recaptcha.net/recaptcha/api.js





#### 密码加密

**Bcrypt** 

使用明文的方式存储账户数据是一个非常严重的安全隐患，要保护用户的密码，就要使用 哈希算法的单向加密方法。 
**哈希算法：**对于相同的数据，哈希算法总是会生成相同的结果。 
**单向加密：**就是信息在加密之后，其原始信息是不可能通过密文反向计算出来的。 
所以，为了账户信息的安全，在数据库中存储的密码应该是被哈希过的哈希值。但是需要注意，哈希算法的种类很多，其中大多是是不安全的，可以被黑客**暴力破解**。 
**暴力破解：**通过遍历各种数据的哈希值，来找到匹配的哈希值，从而获取你的密码权限。 

所以这里我们使用 Bcrypt 哈希算法，这是一种被刻意设计成抵消且缓慢的哈希计算方式，从而极大的加长了暴力破解的时间和成本，以此来保证安全性。



#### 单点登录

#### 认证

CAS系统解决单点登录问题，对身份认证的具体方法不做要求。

OAuth、OpenID、SAML是身份身份认证授权的规范和标准，是解决认证授权问题的。

OpenID强调验证（authentication），而OAuth强调授权（authorization）。 验证就是说“我”是不是(微博用户)，而授权是说“淘宝”可不可以，而可不可以的前提则是“我”是不是。

安全：

- 同源策略
- 

##### JSON对象签名和加密（ JOSE ）

 [JSON Object Signing and Encryption (JOSE)](http://self-issued.info/?p=731)  

Python library

- [authlib](https://github.com/lepture/authlib)
- [pyoidc](https://github.com/rohe/pyoidc)



###### JSON Web Signature (JWS)

 JSON Web 签名，Digital signature/HMAC specification  数字签名/ HMAC规范



###### JSON Web Encryption (JWE)

JSON Web 加密，Encryption specification  加密规范



###### JSON Web Key (JWK)

JSON Web 公钥，Public key specification 公钥规范



###### JSON Web Algorithms (JWA)

JSON Web 算法，Algorithms and identifiers specification  算法和标识符规范



###### JSON Web Token (JWT)

[Java Example](https://www.jianshu.com/p/6307c89fe3fa?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation)

##### OAuth

Oauth协议的认证凭证必须是资源拥有者发放的；而OpenID的认证凭证可以是你需要登录的网站支持的其它任何正规Openid Provier网站均可。

###### V1.0

###### V2.0

##### ~~OpenID~~

~~OpenID Migration to OpenID Connect~~

##### OpenID Connect

OpenID Connect 1.0是OAuth 2.0 [[RFC6749\]](https://openid.net/specs/openid-connect-core-1_0.html#RFC6749) 协议之上的简单身份层 。它使客户端能够根据授权服务器执行的身份验证来验证最终用户的身份，以及以可互操作和类似REST的方式获取有关最终用户的基本配置文件信息。 OpenID Connect对OAuth 2.0进行身份验证的最终用户的主要扩展是ID令牌数据结构。

OpenID Connect对OAuth 2.0进行身份验证的最终用户的主要扩展是ID令牌数据结构。ID令牌是一种安全令牌，其中包含授权服务器在使用客户端时对最终用户的身份验证的声明，以及可能的其他请求的声明。ID令牌表示为[JSON Web令牌（JWT）](https://openid.net/specs/openid-connect-core-1_0.html#JWT) [JWT]

 [OpenID Connect Core 1.0 Specification](https://openid.net/specs/openid-connect-core-1_0.html)  定义了核心OpenID Connect功能：基于OAuth 2.0构建的身份验证以及使用Claims来传达有关最终用户的信息。



```
+--------+                                   +--------+
|        |                                   |        |
|        |---------(1) AuthN Request-------->|        |
|        |                                   |        |
|        |  +--------+                       |        |
|        |  |        |                       |        |
|        |  |  End-  |<--(2) AuthN & AuthZ-->|        |
|        |  |  User  |                       |        |
|   RP   |  |        |                       |   OP   |
|        |  +--------+                       |        |
|        |                                   |        |
|        |<--------(3) AuthN Response--------|        |
|        |                                   |        |
|        |---------(4) UserInfo Request----->|        |
|        |                                   |        |
|        |<--------(5) UserInfo Response-----|        |
|        |                                   |        |
+--------+                                   +--------+
```

OpenID只是身份的象征，可以看作是身份证；而Oauth认证凭证，一定是资源拥有者发放的，不仅是用户在资源拥有者系统身份的凭证，还是其某些授权资源访问的凭证，可以看作是钥匙。

##### SAML

SAML支持XACML协议进行权限控制。SAML协议较OAUTH来说确实比较复杂，但是功能也十分强大，支持认证，权限控制和用户属性。



#### 权限

Python Library: Flask-Principal



### 邮件



```python
from flask import Flask
from flask_mail import Mail, Message

app =Flask(__name__)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'yourId@gmail.com'
app.config['MAIL_PASSWORD'] = '*****'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

@app.route("/")
def index():
   msg = Message('Hello', sender = 'yourId@gmail.com', recipients = ['id1@gmail.com'])
   msg.body = "Hello Flask message sent from Flask-Mail"
   mail.send(msg)
   return "Sent"

if __name__ == '__main__':
   app.run(debug = True)
```



请注意，Gmail服务中的内置不安全功能可能会阻止此登录尝试。您可能必须降低安全级别。请登录您的Gmail帐户并访问[此链接](https://www.google.com/settings/security/lesssecureapps)以降低安全性。

## 数据库



### 版本控制 or 升级降级



大多数情况下，在开始实现一个新项目的代码之前都是先完成数据库的设计。但随着项目新功能的增加或需求变更，不可避免的数据模型的修改会贯穿项目开发的始终。这里会出现一个问题：当把这些数据模型的更新从开发环境迁移到生产环境时，怎样才能在保证原有数据完整性的情况下更新的数据库结构或将数据库回滚到之前的某一个时刻以便复现环境。

**Alembic**(Database migration 数据迁移跟踪记录) 提供的数据库升级和降级的功能，就可以帮我们解决上述的问题。它所能实现的效果有如 Git 管理项目代码一般。 [[ref](https://blog.csdn.net/jmilk/article/details/53241361)]







Salt

nova



## 单元测试

## BUG跟踪

## 版本控制



# 运维



[Nagios](https://www.nagios.com/)



pipenv   and  pip

https://packaging.python.org/guides/distributing-packages-using-setuptools/#distributing-packages

https://github.com/pypa/sampleproject



## 案例



https://www.stratoscale.com/blog/devops/using-jenkins-build-deploy-python-web-applications-part-2/

https://medium.com/@anirbanroydas/testing-microservice-written-in-python-flask-with-continuous-integration-delivery-and-deployment-1999fef560a8

https://my.oschina.net/xxjbs001/blog/361736

https://dzone.com/articles/setting-up-jenkins-cicd-for-python-flask-docker-ap

https://circleci.com/blog/continuously-deploying-python-packages-to-pypi-with-circleci/

http://flagzeta.org/blog/a-python-microservice-stack/







