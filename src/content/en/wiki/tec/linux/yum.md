project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Linux

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}
{# wf_blink_components: Platform>DevTools #}

# YUM Repostory

Repo List

    yum repolist
    yum repolist all

Repo enable

    yum --enablerepo=elrepo-kernel install kernel-ml

Repo disable

    yum-config-manager --disable mysql57-community

Repo For Local File System

    mount -o loop /dev/cdrom /var/www/html/CentOS/

Touch media repo:

    [c7-media]
    name=CentOS-$releasever - Media
    baseurl=file:///media/CentOS7/
    http://10.252.108.111/CentOS7/
    gpgcheck=1
    enabled=1
    gpgkey=http://10.252.108.111/CentOS7/RPM-GPG-KEY-CentOS-7

Extention Remote

    http://mirrors.aliyun.com/repo/

    http://mirrors.163.com/centos/

    http://mirrors.sohu.com/centos/

Repo Usage

    yum --disablerepo=remi-safe  update

    yum --disablerepo="*" --enablerepo="elrepo-kernel" list available

    yum --disablerepo=\* --enablerepo=elrepo-kernel install kernel-ml

Ref: Kernel

Yum History

    yum history list git
    yum history undo 13

## YUM Proxy

vi /etc/yum.conf

    proxy=http://10.252.108.115:3128

## Extension Mirror

epel

    https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

rmfushion

    http://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm

    yum install gstreamer-ffmpeg

RPMForge

    http://repository.it4i.cz/mirrors/repoforge/redhat/el7/en/x86_64/rpmforge/RPMS/rpmforge-release-0.5.3-1.el7.rf.x86_64.rpm


