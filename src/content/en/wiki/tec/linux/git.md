project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Git

{# wf_updated_on: 2018-12-18 #}
{# wf_published_on: 2018-12-18 #}
{# wf_blink_components: N/A #}

# Git {: .page-title }

[Git Documentation](https://git-scm.com/doc)

Git Server

Config Account

```shell
git config --list
git config --global user.email "zhengxiongzhao@gmail.com"
git config --global user.name "xxxx"


git config --global https.proxy http://127.0.0.1:1080
git config --global https.proxy https://127.0.0.1:1080
git config --global http.proxy 'socks5://127.0.0.1:1086'
git config --global https.proxy 'socks5://127.0.0.1:1086'

git config --global --unset http.proxy
git config --global --unset https.proxy
```

Push New Project  

```shell
cd projectDir
git init
git add .
git commit -a -m `init repos`
git remote add origin https://github.com/zmasao/JavaSamples.git
git push origin master
```


Branch

```
git branch                              # list branch
git branch dev                          # create branch
git checkout dev                        # switch branch
git checkout -b dev                     # create and switch branch
git checkout master

git branch -a                           # list remote and local branches
git branch -r                           # list remote branches
git branch -d dev                       # delete branch

git merge dev                           # merge branch

git push -u origin dev                  # push dev branch

git push origin :dev                  
git push origin –delete dev             # delete remote branch
```

Problom

ERROR hint: Updates were rejected because the tip of your current branch is behind

```
git pull origin master
```

ERROR fatal: refusing to merge unrelated histories

```
git pull origin master --allow-unrelated-histories
git push origin master
```

# push success

```
mkdir nodebook
cd nodebook/
git init
echo "Hello World" > README
git add README
git commit -a -m "Init Repos"
git remote add origin git@192.168.1.125:notebook.git
git push origin master
Git init -bare
```

Example 1

```
~/project $ git init
~/project $ git add .
~/project $ git commit -m "first commit"
~/project $ cd ~/Google\ Drive/git

~/Google\ Drive/git $ git init --bare project.git
~/Google\ Drive/git $ cd ~/project

~/project $ git remote add origin ~/Google\ Drive/git/project.git
~/project $ git push -u origin master
~/project $ cd /tmp
~/tmp $ git clone ~/Google\ Drive/git/project.git
~/tmp $ cd project
~/tmp/project $ 
```

Reference  

[1]. [GitWiki](https://git.wiki.kernel.org/index.php/Main_Page)  
[2]. Git 个人仓库  
[3]. Git 使用教程 4  
[4]. Git 使用教程 5   
[5]. Git 使用教程 6  



