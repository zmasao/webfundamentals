project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: CentOS 7

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}



# CentOS 7 {: .page-title }

## Configuration
### System Configuration

#### Mirror Configuration

Ref: [YUM](yum)

#### Network Configuration

##### DHCP

```sh
ip addr
ifup enp0s3
```

##### STATIC

Input:

    ip addr
    vi /etc/sysconfig/network-scripts/ifcfg-enp0s3

Edit:

```ini
BOOTPROTO=static
ONBOOT=yes
IPADDR=192.168.10.128
GATEWAY=192.168.10.254
DNS1=8.8.8.8
DNS1=8.8.4.4
```

start network interface or restart network:

    ifup  enp0s3
    systemctl restart network.service




### Desktop Configuration

#### Configuring Automatic Login

CentOS 7 Desktop Migration and Administration Guide

Edit the `/etc/gdm/custom.conf` file and make sure that the `[daemon]` section in the file specifies the following:

    [daemon]
    AutomaticLoginEnable=True
    AutomaticLogin=zxzhao

Replace john with the user that you want to be automatically logged in.

#### Numix Theme

    open Tweaks  -  Extensions  -  User themes
    sudo yum install -y numix-icon-theme-circle
    close Tweaks

#### Playonlinux

    sudo wget -O /etc/yum.repos.d/playonlinux.repo http://rpm.playonlinux.com/playonlinux.repo
    sudo yum install -y wine wxPython cabextract SDL.i686 p7zip-plugins p7zip ImageMagick libXaw xterm mesa-dri-drivers.i686 mesa-libGL.i686 libGLEW.i686 mesa-libGLU.i686 nc
    sudo yum install -y playonlinux
    yum install samba-winbind-clients

#### Photoshop

[ref](https://www.youtube.com/redirect?q=https%3A%2F%2Fhelpx.adobe.com%2Fx-productkb%2Fpolicy-pricing%2Fcs6-product-downloads.html&redir_token=LStM6fnJv7ywnpuYGcN0IFJGFjN8MTUwNjE1MTU3NUAxNTA2MDY1MTc1&event=desc)

Photoshop CS6, Photoshop CS6 Extended
Windows

#### Gif Record



```
yum install -y meson gtk3-devel keybinder txt2man
git clone https://github.com/phw/peek.git
cd peek
mkdir build
cmake -DCMAKE_INSTALL_PREFIX=~/.local/stow/peek build
make
make install
```



shell

```
#!/bin/sh

set -x

mkdir -p ~/.local/src/
cd ~/.local/src/

if [ ! -d peek ]
then
    git clone --depth 1 https://github.com/phw/peek.git
fi
mkdir peek/build
cd peek/build
cmake -DCMAKE_INSTALL_PREFIX=~/.local/stow/peek ..
make
make install


cd ~/.local/stow
stow peek
```



 

#### Could Music 163

1. Download

2. Requerement

        yum install epel-release
        yum http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
        rpm -Va --nofiles --nodigest http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm
        rpm -Va --nofiles —nodigest  http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
        yum -y --enablerepo=nux-dextop install gstreamer-ffmpeg vlc gstreamer-plugins-ugly gstreamer-plugins-bad gstreamer-plugins-ugly ffmpeg libvdpau mpg123 mplayer mplayer-gui gstreamer-plugins-bad-nonfree gstreamer1-libav gstreamer1-plugins-bad-freeworld gstreamer1-plugins-ugly
        yum install dbus-libs

    Warning: update glibc and libstdc++**

3. unzip

        mkdir netease-cloud-music; mv netease-cloud-music_1.0.0-2_amd64_ubuntu16.04.deb netease-cloud-music/; cd netease-cloud-music
        ar -x netease-cloud-music_1.0.0-2_amd64_ubuntu16.04.deb; tar Jxf data.tar.xz

4. installation

        sudo cp -r usr/* /usr/

5. running

        /usr/bin/netease-cloud-music

6. 解决abort问题

        sudo chmod 4755 /usr/lib/netease-cloud-music/chrome-sandbox

### Virtual



#### VirtualBox

Requirement 

```
yum install -y kernel-headers kernel-devel
```

[Download VirtualBox for Linux Hosts](https://www.virtualbox.org/wiki/Linux_Downloads)

Oracle Linux 7 / Red Hat Enterprise Linux 7 / CentOS 7

```
rpm -ivh https://download.virtualbox.org/virtualbox/6.0.0/VirtualBox-6.0-6.0.0_127566_el7-1.x86_64.rpm
/sbin/vboxconfig
```



#### Installing VirtualBox Additions

1. Install Additions Environment on hree.

        $ yum -y install epel-release 
        $ yum -y update kernel*  
        $ yum -y install gcc gcc-c++ kernel-devel kernel-headers dkms make bzip2  
        $ rpm -qa | grep kernel 
        $ rpm -e kernel-3.10.0-229.el7.x86_64 
        $ rpm -qa | grep kernel 
        $ reboot

2. Mount Additions CD

        mount /dev/cdrom /media/cdrom/

3. Input a command like below after finishing installation:

        export KERN_DIR=/usr/src/kernels/3.10.0-327.36.3.el7.x86_64
        sh VBoxLinuxAdditions.run

Warning: vboxadd.sh: Building Guest Additions kernel modules.  Failed to set up service vboxadd, please check the log file

    ln -s /usr/src/kernels/3.10.0-327.36.3.el7.x86_64 /usr/src/linux # sh VBoxLinuxAdditions.run

Warning: vboxadd.sh: failed: modprobe vboxguest failed.

    yum -y update kernel*

Warning: Share Folder "This location could not be displayed"

    usermod -a -G vboxsf zxzhao



## Problem


#### sysctl: permission denied on key

修复modprobe

    rm -f /sbin/modprobe
    ln -s /bin/true /sbin/modprobe

修复sysctl

    rm -f /sbin/sysctl
    ln -s /bin/true /sbin/sysctl

#### glibc update

    curl -O http://ftp.gnu.org/gnu/glibc/glibc-2.18.tar.gz
    tar zxf glibc-2.18.tar.gz; cd glibc-2.18/
    mkdir build
    cd build
    ../configure --prefix=/usr
    make -j16 && sudo make install

#### version `GLIBCXX_3.X.X' not found

1. check glibcxx version

        strings /usr/lib64/libstdc++.so.6|grep GLIBCXX
        GLIBCXX_3.4.18    GLIBCXX_3.4.19    GLIBCXX_DEBUG_MESSAGE_LENGTH

2. update libstdc++

        wget ftp://ftp.pbone.net/mirror/archive.fedoraproject.org/fedora/linux/updates/22/x86_64/l/libstdc++-5.3.1-6.fc22.x86_64.rpm
        rpm2cpio ./libstdc++-5.3.1-6.fc22.x86_64.rpm | cpio -id
        cp usr/lib64/libstdc++.so.6.0.21 /usr/lib64/
        ln -sf /usr/lib64/libstdc++.so.6.0.21 /usr/lib64/libstdc++.so.6

3. check glibcxx version

        strings /usr/lib64/libstdc++.so.6|grep GLIBCXX
        GLIBCXX_3.4.20    GLIBCXX_3.4.21    GLIBCXX_DEBUG_MESSAGE_LENGTH

