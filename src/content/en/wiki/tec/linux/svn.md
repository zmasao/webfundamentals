

# 创建版本库



svnadmin create ~/.SVNRepository

Subversion目录说明：

```
db目录：所有版本控制的数据存放文件。
hooks目录：放置hook脚本文件的目录。
locks目录：用来追踪存取文件库的客户端。
format文件：是一个文本文件，里面只放了一个整数，表示当前文件库配置的版本号。
conf目录：是这个仓库的配置文件（仓库的用户访问账号、权限等）。
	authz：是权限控制文件。
	passwd：是账号密码文件。
	svnserve.conf：SVN服务配置文件。
```

svnserve.conf

```
anon-access = read #匿名用户可读，您也可以设置 anon-access = none，不允许匿名用户访问。设置为 none，可以使日志日期正常显示
auth-access = write #授权用户可写
password-db = passwd #使用哪个文件作为账号文件
authz-db = authz #使用哪个文件作为权限文件
realm = /var/svn/svnrepos #认证空间名，版本库所在目录
```

passwd

```
[users]
zxzhao = qwe123
```



authz

```
[/]
zxzhao=rw
```

Start Server

```
svnserve -d -r /var/svn/svnrepos
```

 




https://172.16.1.21/svn/101_BIO_NGS/

svn list https://172.16.1.21/svn/101_BIO_NGS/

svn list https://172.16.1.21/svn/

svn list file://$HOME/.SVNRepository

svn: E175002: Unable to connect to a repository at URL 'https://172.16.1.21/svn'
svn: E175002: The OPTIONS request returned invalid XML in the response: XML parse error at line 1: no element found (https://172.16.1.21/svn)



## Subversion 目录规范

- trunk
- branches
- tags



svn mkdir $svnrepos/{trunk,branches,tags}



# Command-Line

[svn help (h, ?)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.help.html)

[svn add](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.add.html)

[svn commit (ci)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.commit.html)

[svn checkout (co)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.checkout.html)

[svn update (up)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.update.html)

[svn delete (del, remove, rm)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.delete.html)

[svn status (stat, st)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.status.html)   

' ' 没有修改 
A 被添加到本地代码仓库 
C 冲突 
D 被删除 
I 被忽略 
M 被修改 
R 被替换 
X 外部定义创建的版本目录 
? 文件没有被添加到本地版本库内 
!  文件丢失或者不完整(不是通过svn命令删除的文件) 
~ 受控文件被其他文件阻隔 

[svn diff (di)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.diff.html)   -r      [Revision Keywords](http://svnbook.red-bean.com/nightly/en/svn.tour.revs.specifiers.html#svn.tour.revs.keywords)



[svn revert](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.revert.html)

[svn log](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.log.html)

[svn resolve](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.resolve.html)



[svn copy (cp)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.copy.html)

[svn move (mv)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.move.html)

[svn mkdir](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.mkdir.html)

[svn info](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.info.html)

[svn cat](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.cat.html)

[svn list (ls)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.list.html)





[svn upgrade](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.upgrade.html)

```
$ svn status
svn: E155036: Please see the 'svn upgrade' command
svn: E155036: Working copy '/home/zxzhao/project' is too old (format 10, create
d by Subversion 1.6)
```

[svn import](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.import.html)

[svn export](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.export.html) 检出项目，不是工作副本，没有.svn文件内容





[svn changelist (cl)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.changelist.html)

[svn blame (praise, annotate, ann)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.blame.html)

[svn patch](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.patch.html)

[svn merge](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.merge.html)

[svn mergeinfo](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.mergeinfo.html)

[~~svn resolved~~](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.resolved.html) 删除工作副本文件或目录上的“ 冲突 ”状态。

[svn switch (sw)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.switch.html)

`--relocate` option is deprecated as of Subversion 1.7. Use svn relocate



[svn relocate](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.relocate.html)

[svn lock](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.lock.html)

[svn unlock](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.unlock.html)

[svn cleanup](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.cleanup.html)



[svn propdel (pdel, pd)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.propdel.html)

$ svn propdel svn:mime-type some-script
property 'svn:mime-type' deleted from 'some-script'.

[svn propedit (pedit, pe)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.propedit.html)

[svn propget (pget, pg)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.propget.html)

[svn proplist (plist, pl)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.proplist.html)

[svn propset (pset, ps)](http://svnbook.red-bean.com/nightly/en/svn.ref.svn.c.propset.html)

$ svn propset svn:mime-type image/jpeg foo.jpg 
property 'svn:mime-type' set on 'foo.jpg'

$ svn propset --revprop -r 25 svn:log "Journaled about trip to New York."
property 'svn:log' set on repository revision '25'



svn propset svn:ignore bin .

svn propset svn:ignore -R *.class .

svn propset svn:ignore -R "*.class

\> *.apk
\> Thumbs.db" .

svn propset svn:ignore -R -F .svnignore .

svn status --no-ignore



https://www.open.collab.net/scdocs/ddUsingSVN_command-line.html.zh-cn

# Example

mkdir chrgraph
cd chrgraph/
touch README.rst


svn mkdir file:///home/zxzhao/.SVNRepository/{CODE,DOC} -m 'init basic directory!'

[zxzhao@localhost ~]$ svn list file:///home/zxzhao/.SVNRepository/
CODE/
DOC/

[zxzhao@localhost ~]$ svn import chrgraph file:///home/zxzhao/.SVNRepository/CODE/chrgraph -m 'init chrgraph project.'
Adding         chrgraph/README.rst

[zxzhao@localhost ~]$ rm -rf chrgraph/
[zxzhao@localhost ~]$ svn co file:///home/zxzhao/.SVNRepository/CODE/chrgraph
[zxzhao@localhost chrgraph]$ svn info
Path: .
Working Copy Root Path: /home/zxzhao/Documents/workspace/chrgraph
URL: https://172.16.1.21/svn/101_BIO_NGS/CODE/chrgraph
Repository Root: https://172.16.1.21/svn/101_BIO_NGS
Repository UUID: c2a7474a-0d97-4743-b204-da8d2b76eb12
Revision: 78
Node Kind: directory
Schedule: normal
Last Changed Author: Zhaozx
Last Changed Rev: 78
Last Changed Date: 2019-01-23 18:00:43 +0800 (Wed, 23 Jan 2019)

[zxzhao@localhost chrgraph]$ vi README.rst 
chrgraph
\==============
[zxzhao@localhost chrgraph]$ svn st
M       README.rst

[zxzhao@localhost chrgraph]$ svn diff README.rst 
Index: README.rst
\===================================================================
--- README.rst	(revision 3)
+++ README.rst	(working copy)
@@ -0,0 +1,2 @@
+chrgraph



[zxzhao@localhost chrgraph]$ svn ci README.rst -m 'initial README'


[zxzhao@localhost chrgraph]$ svn st



[zxzhao@localhost chrgraph]$ svn cat file:///home/zxzhao/.SVNRepository/CODE/chrgraph/README.rst 
chrgraph
\==============
[zxzhao@localhost chrgraph]$ svn delete 
[zxzhao@localhost chrgraph]$ svn cp README.rst README2.rst 
A         README2.rst
[zxzhao@localhost chrgraph]$ svn st
M       README.rst
A  +    README2.rst
[zxzhao@localhost chrgraph]$ svn ci -m 'copy README.rst to README2.rst'
Sending        README.rst
Adding         README2.rst
Transmitting file data ..
Committed revision 5.



[zxzhao@localhost chrgraph]$ vi README.rst 
chrgraph project
\==============

[zxzhao@localhost chrgraph]$ svn diff README.rst 
Index: README.rst
\===================================================================
--- README.rst	(revision 4)
+++ README.rst	(working copy)
@@ -1,2 +1,5 @@
-chrgraph

+chrgraph project
 \==============





Check out tree
$ svn co http://svn.somewhere.com/repos
A  Makefile
A  document.c
A  search.c
Checked out revision 4.
Edit search.c
$ vi search.c
Commit the edit
$ svn commit –m “Add better search”
Sending search.c
Transmitting data…
Committed revision 5.