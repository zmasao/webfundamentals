project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: Firewall

{# wf_updated_on: 2018-06-18 #}
{# wf_published_on: 2016-03-28 #}

# Firewall {: .page-title }

### firewalld-cmd

    firewall-cmd --list-all
    firewall-cmd --reload

    firewall-cmd --list-services

    --permanent

    firewall-cmd --add-port=80/tcp
    firewall-cmd --remove-port=80/tcp
    firewall-cmd --add-port=8880-8889/tcp

    firewall-cmd --add-service=samba


### Firewalld Service Port Change

    vi /usr/lib/firewalld/services/ssh.xml
    firewall-cmd --reload


### Port forward

    sysctl net.ipv4.ip_forward

    firewall-cmd --add-masquerade
    firewall-cmd --add-forward-port=port=80:proto=tcp:toaddr=10.252.102.33:toport=5080
    firewall-cmd --add-forward-port=port=8080:proto=tcp:toaddr=10.252.102.81:toport=8080
    firewall-cmd --reload

    firewall-cmd --runtime-to-permanent
