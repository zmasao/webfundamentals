project_path: /web/wiki/_project.yaml
book_path: /web/wiki/_book.yaml
description: GCloud

{# wf_updated_on: 2018-12-18 #}
{# wf_published_on: 2018-12-18 #}
{# wf_blink_components: N/A #}

# GCloud {: .page-title }

[Reference Google Cloud SDK Documentation](https://cloud.google.com/sdk/docs/)

## Installtion and Configuration

### Quick Start RedHat CentOS

Red Hat Enterprise Linux 7 and CentOS 7 support Cloud SDK RPM packages [[ref]](https://cloud.google.com/sdk/docs/quickstart-redhat-centos#before-you-begin)

```shell
# Update YUM with Cloud SDK repo information:
sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
[google-cloud-sdk]
name=Google Cloud SDK
baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOM

# The indentation for the 2nd line of gpgkey is important.

# Install the Cloud SDK
yum install -y google-cloud-sdk google-cloud-sdk-app-engine-python google-cloud-sdk-datastore-emulator
```

### Quick Start Mac

https://cloud.google.com/sdk/docs/quickstart-macos

```shell
$ wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-202.0.0-darwin-x86_64.tar.gz

$ ./google-cloud-sdk/install.sh
```


## GCloud Command

### GCloud Proxy

Proxy Type ： `http, http_no_tunnel, socks4, socks5`

```shell
$ gcloud config set proxy/type socks5
$ gcloud config set proxy/address 127.0.0.1
$ gcloud config set proxy/port 1086
```

### GCloud Deploy

Project URI : `https://console.cloud.google.com/storage/browser?project=soul-b2861`

```shell
$ gcloud app deploy app.yaml --project soul-b2861
$ gcloud app deploy 
```

Setting traffic split for service [default]...done.   
Deployed service [default] to [https://soul-b2861.appspot.com]

You can stream logs from the command line by running:
```
$ gcloud app logs tail -s default
```

To view your application in the web browser run:
```
$ gcloud app browse --project=soul-b2861
```

### GCloud Components

```shell
$ gcloud components list
$ gcloud components install COMPONENT_ID
$ gcloud components remove COMPONENT_ID
$ gcloud components update
```


